// contracts/TKN.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Capped.sol";

contract TKN is ERC20Capped {
    constructor(uint256 cap)
        ERC20Capped(cap) 
        ERC20("TKN", "TKN")
    {
        _mint(_msgSender(), cap);
    }

    function mint() public {
        _mint(address(this), 1000);
    }

    function burn(uint256 amount) public {
        _burn(_msgSender(), amount);
    }
}