// declare module "@likelib/core" {
//     const Account = {
//         createAccount(): {
//             getPrivKey(): string;
//             getAddress(): string;
//         };
//     };
// }


export default class Likelib {
    static Account = class Account {
        constructor(private_key: string);
        static createAccount(): Account;
        getPrivKey(): string;
        getAddress(): string;
    }

    static Contract = class Contract {
        static deployed(lk: Likelib, account: Account, abi: any, address: string): Contract;
        static nondeployed(lk: Likelib, account: Account, abi: any, bytecode: string): Contract;

        constructor();
        deploy(...args);
        _setupMethods(abi: string);
    }

    constructor(url: string);
}
