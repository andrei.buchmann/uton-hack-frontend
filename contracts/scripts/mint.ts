import LikeLib from "@likelib/core";
let eip55 = require('eip55');
const bs58 = require('bs58');

function sleep() {
    var delay = 2000
    delay += new Date().getTime();
    while (new Date() < (delay as any)) { }
}

const TKN = require("../build/contracts/TKN.json");

const url =  "ws://51.250.18.124:50051";
let lk = new LikeLib(url);

const accountMain = new LikeLib.Account("2aef91bc6d2df7c41bd605caa267e8d357e18b741c4a785e06650d649d650409");
const account = LikeLib.Account.createAccount();


const abi = TKN.abi;
const bytecode = TKN.bytecode.slice(2);

const contractAddr = "3TQAmYNE6TvsarSaNbPK6kg2YZpR";
const contract = LikeLib.Contract.deployed(lk, accountMain, abi, contractAddr);
contract._setupMethods(abi);


const addrEip55 = eip55.encode(bs58.decode(contractAddr).toString('hex'));
console.log(addrEip55);

(contract as any).transfer(addrEip55, "100", 0, 100000, function(err: any, result: any) {
    if (err) {
        console.log("Error occured", err);
    } else {
        console.log("Ok.", result);
    }
});

(contract as any).balanceOf(addrEip55, 0, 5000, function(err: any, result: any) {
    if (err) {
        console.log("Error occured", err);
    } else {
        console.log("Ok.", result);
    }
});