const LikeLib = require("@likelib/core");
const NFT = require("../build/contracts/InfiniteHero.json");

const url =  "ws://51.250.18.124:50051";
let lk = new LikeLib(url);

const account = new LikeLib.Account("2aef91bc6d2df7c41bd605caa267e8d357e18b741c4a785e06650d649d650409");

const abi = NFT.abi;
const bytecode = NFT.bytecode.slice(2);


const contract = LikeLib.Contract.nondeployed(lk, account, abi, bytecode);

contract.deploy(0, 10000000, function(err, fee_left) {
  if (err) {
    console.log(err);
  } else {
    console.log("Contract was successfully deployed fee_left: " + fee_left);
    console.log("Contract address: " + contract._address + " Set it address in contract call");
  }
})