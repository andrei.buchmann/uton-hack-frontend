import $api from '../http/index';

export default class AuthService {
    static async login(email, password) {
        return $api.post('/user/login', {email, password})
    }

    static async register(newUserBody) {
        return $api.post('/user/register', newUserBody)
    }

    static async logout() {
        return $api.post('/user/logout')
    }
}