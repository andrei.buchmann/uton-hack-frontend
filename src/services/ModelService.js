import $api from '../http/index';

export default class ModelService {
    static async getById(id) {
        return $api.get(`/model/getById/${id}`)
    }

    static async updateModelPrintedStatus(id, printedStatus) {
        return $api.post(`/model/updateModelPrintedStatus/${id}`, {printed: printedStatus})
    }

}