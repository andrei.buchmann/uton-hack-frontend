import $api from '../http/index';

export default class UserService {
    static async updateUserTokenAmount(id, tokenAmount, walletTokenAmount) {
        return $api.post(`/user/updateUserTokenAmount/${id}`, {tokenAmount, walletTokenAmount})
    }

    static async getUserById(id) {
        return $api.get(`/user/getUserById/${id}`)
    }
}