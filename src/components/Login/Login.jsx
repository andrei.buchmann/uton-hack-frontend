import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import './Login.css';
import {Link} from 'react-router-dom'
import {useDispatch, useSelector} from 'react-redux';
import {loginUserAction} from "../../store/user/action";
import FormAlert from "../FormAlert/FormAlert";

export default function Login() {
    const {
        failure,
        message,
    } = useSelector((state) => state.user);

    const dispatch = useDispatch();

    const [formState, setFormState] = React.useState({
        emailInput: '',
        passwordInput: '',
        rememberStatusInput: false
    })

    const [formAlert, setFormAlert] = React.useState({
        message: '',
        status: false
    });

    const onChangeInput = (e) => {
        setFormState({
            ...formState,
            [e.target.name]: e.target.value,
        })
    }

    const handleSubmit = async e => {
        e.preventDefault();

        dispatch(loginUserAction({
            email: formState.emailInput,
            password: formState.passwordInput
        }));
    }

    return (
        <div>
            <form className="mb-3" onSubmit={handleSubmit}>
                <h3>Login</h3>

                <div className="mb-3">
                    <div className="form-group">
                        <label>Email address</label>
                        <input
                            onChange={onChangeInput}
                            type="email"
                            name="emailInput"
                            className="form-control"
                            placeholder="Enter email"
                            required
                        />
                    </div>
                </div>

                <div className="mb-3">
                    <div className="form-group">
                        <label>Password</label>
                        <input
                            onChange={onChangeInput}
                            type="password"
                            name="passwordInput"
                            className="form-control"
                            placeholder="Enter password"
                            required
                        />
                    </div>
                </div>

                <button
                    type="submit"
                    className="btn btn-primary btn-block mb-3"
                >
                    Submit
                </button>
                <FormAlert/>
            </form>
            <Link to={"./register"}>
                Register
            </Link>
        </div>
    );
}