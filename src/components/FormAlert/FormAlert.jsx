import Alert from '@mui/material/Alert';
import {useSelector} from "react-redux";

export default function FormAlert() {
    const {
        failure,
        message,
    } = useSelector((state) => state.user);

    if (failure) {
        return (
            <Alert severity="error" className="mb-3">
                {message}
            </Alert>
        )
    } else {
        return (
            <></>
        )
    }
}