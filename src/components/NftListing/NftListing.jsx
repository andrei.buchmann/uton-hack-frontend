import React from 'react';
import './NftListing.css';
import {Link} from "react-router-dom";
import {Alert} from "@mui/material";
import nftTokenService from '../../services/NftTokenService';
import {useSelector} from "react-redux";

export default function NftListing() {
    const [nfts, setNfts] = React.useState([]);

    const {
        walletAddress,
    } = useSelector((state) => state.user);

    React.useEffect(() => {
        console.log('nfts from useEffect: ', nfts)
    }, [nfts])

    React.useEffect(async () => {
        async function getAllNftsFromServer() {
            let emailString = localStorage.getItem('email');
            await nftTokenService.getAllNftsByUserEmail(emailString).then(res => {
                console.log('res.data: ', res.data)
                setNfts((_) => res.data)
            })
                .catch((err) => {
                    console.log(err.data)
                })
        }

        getAllNftsFromServer();
    }, [])

    return (
        <div className="auth-inner">
            <div className="list-group">
                {nfts.length === 0
                    ?
                    <div className="btn__play">
                        <Alert severity="success" className="mb-3">
                            So far no NFTs on Your account. Play and Create Heroes to Mint them
                        </Alert>
                        <Link className="btn btn-primary" to={"./"}>
                            Play
                        </Link>
                    </div>
                    :
                    nfts.map((nft) => {
                            console.log(nft)
                            return (
                                <div key={nft.token_id}
                                     className="list-group-item list-group-item-action flex-column align-items-start">
                                    <div className="d-flex w-100 justify-content-between mt-5">
                                        <h5 className="mb-1">{nft.name}</h5>
                                        <small>{nft.contract_type}</small>
                                    </div>
                                    <ul className="list-group list-group-flush mb-3">
                                        <li className="list-group-item">Owner address: {walletAddress}</li>
                                        <li className="list-group-item">Symbol: IHERO</li>
                                        <li className="list-group-item">Token Address: {nft.token_id}</li>
                                        <li className="list-group-item">Token ID: {nft._id}</li>

                                        {nft.description ? <li className="list-group-item">Token
                                            Description: {nft.description}</li> : <></>}
                                        {nft.name ?
                                            <li className="list-group-item">Token Name: {nft.name}</li> : <></>}
                                    </ul>
                                    {
                                        nft.image_uri
                                            ?
                                            <div className="nft_listing__image">
                                                <img
                                                    src={nft.image_uri}
                                                    alt="Card image cap"
                                                />
                                            </div>
                                            :
                                            <></>
                                    }
                                </div>
                            )
                        }
                    )
                }
            </div>
        </div>
    )
}