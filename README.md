## Play and Earn $GLRY, withdraw on DEX
## Create unique Hero, save statistics in NFT and exchange on marketplaces
## Use NFTs not only in one game, but in metaverse
## Use Clover Ecosystem for game growing

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## To test NFT Mint it's necessary to connect to test blockchain nest
Network parameters when creating new RPC network with LikeLib

Frontend part with Clover blockchain integrations - (ссылка на гитхаб)

url of the node ws://51.250.18.124:50051

IHERO NFT address - 3EpDH19xHP9WP4adz5STciVYYbH1
GLRY ERC-20 token address - 3TQAmYNE6TvsarSaNbPK6kg2YZpR

Play and Earn in-game coins, witdraw them into $GLRY, exchange on DEX
Create unique Hero, upgrade him in battles, save statistics in $IHERO NFT and sell it on NFT marketplaces
Use NFTs not only in one game, but in metaverse
Use Clover Ecosystem for game growing

In the project directory, you can run:

### Install Dependencies and Run the Frontend Part

```
$ npm install
$ npm run start
```

First time the game might take some time to load